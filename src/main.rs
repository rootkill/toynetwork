mod errors;
mod scope;

use derive_more::{Display, Error};
use errors::Error;
use futures::{
    channel::oneshot,
    future::{ready, select},
    stream,
};
use log::info;
use ntex::{
    chain, fn_service, rt,
    service::{fn_factory_with_config, fn_shutdown},
    util::Bytes,
    web, Service,
};
use ntex_files::NamedFile;
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};
use scope::scoped_config;
use serde::Deserialize;
use std::{
    borrow::{Borrow, BorrowMut},
    cell::{Cell, RefCell},
    rc::Rc,
    sync::{atomic::AtomicUsize, Arc, Mutex},
    task::Poll,
    time::{Duration, Instant},
};

#[derive(Clone)]
struct ReadOnlyAppState {
    count: Cell<usize>,
    global_count: Arc<AtomicUsize>,
}

struct AppName {
    name: String,
}

#[derive(Clone)]
struct AppStateCounter {
    counter: Arc<Mutex<u32>>,
}

#[derive(Debug, Deserialize)]
struct Info {
    username: String,
}

#[web::get("/")]
async fn index() -> impl web::Responder {
    web::HttpResponse::Ok()
}

#[web::get("/hello")]
async fn hello(name: String) -> impl web::Responder {
    web::HttpResponse::Ok().body(format!("Hello {}", name))
}

#[web::post("/echo")]
async fn echo(req_body: String) -> impl web::Responder {
    web::HttpResponse::Ok().body(req_body)
}

async fn manual_hello() -> impl web::Responder {
    web::HttpResponse::Ok().body("Hey there")
}

#[web::get("/json")]
async fn handle_json(data: web::types::Json<Info>) -> impl web::Responder {
    log::info!("Received JSON: {:?}", data);
    log::info!("Username: {:?}", data.username);
    web::HttpResponse::Ok().body(format!("Username is: {}", data.username))
}

#[web::get("/counter")]
async fn increment_counter(state: web::types::State<AppStateCounter>) -> impl web::Responder {
    let mut counter = state.counter.lock().unwrap();
    *counter += 1;
    format!("Counter: {counter}\r\n")
}

#[web::get("/readonlyappstate")]
async fn show_read_only_app_state(
    data: web::types::State<ReadOnlyAppState>,
) -> impl web::Responder {
    format!(
        "Read Only local app state count: {}\nGlobal app state count: {}",
        data.count.get(),
        data.global_count.load(std::sync::atomic::Ordering::Relaxed)
    )
}

#[web::post("/add_one_to_ro_app_state")]
async fn add_one(data: web::types::State<ReadOnlyAppState>) -> impl web::Responder {
    let mut count = data.count.get();

    data.global_count
        .fetch_add(1, std::sync::atomic::Ordering::Relaxed);

    count += 1;

    data.count.set(count);

    format!(
        "Updated local state is: {}\nUpdated Global State is: {}",
        data.count.get(),
        data.global_count.load(std::sync::atomic::Ordering::Relaxed)
    )
}

#[derive(Debug, Display, Error)]
enum UserError {
    #[display(fmt = "An internal error occurred. Please try again later.")]
    InternalError,
}

impl web::error::WebResponseError for UserError {
    fn error_response(&self, _: &web::HttpRequest) -> web::HttpResponse {
        web::HttpResponse::build(self.status_code())
            .set_header("content-type", "text/html; charest=utf-8")
            .body(self.to_string())
    }

    fn status_code(&self) -> ntex::http::StatusCode {
        match *self {
            UserError::InternalError => ntex::http::StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

#[web::get("/error")]
async fn handle_error() -> Result<&'static str, UserError> {
    String::from("1O")
        .parse::<u128>()
        .map_err(|_e| UserError::InternalError)?;

    Ok("Success!")
}

async fn sse(_req: web::HttpRequest) -> web::HttpResponse {
    let mut counter: usize = 5;

    let server_events = stream::poll_fn(
        move |_cx| -> Poll<Option<Result<ntex::util::Bytes, web::Error>>> {
            if counter == 0 {
                return Poll::Ready(None);
            }

            let payload = format!("data: {}\r\n", counter);

            counter -= 1;

            Poll::Ready(Some(Ok(Bytes::from(payload))))
        },
    );

    web::HttpResponse::build(ntex::http::StatusCode::OK)
        .set_header(ntex::http::header::CONTENT_TYPE, "text/event-stream")
        .set_header(ntex::http::header::CONTENT_ENCODING, "identity")
        .streaming(server_events)
}

async fn file_server(req: web::HttpRequest) -> Result<ntex_files::NamedFile, web::Error> {
    let path: std::path::PathBuf = req.match_info().query("filename").parse()?;
    Ok(NamedFile::open(path)?)
}

#[ntex::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "info");
    std::env::set_var("RUST_BACKTRACE", "1");
    env_logger::init();

    let counter = AppStateCounter {
        counter: Arc::new(Mutex::new(0)),
    };

    let ro_counter = ReadOnlyAppState {
        count: Cell::new(100),
        global_count: Arc::new(AtomicUsize::new(100)),
    };

    let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();

    builder
        .set_private_key_file("key.pem", SslFiletype::PEM)
        .unwrap();

    builder.set_certificate_chain_file("cert.pem").unwrap();

    web::HttpServer::new(move || {
        let logger = web::middleware::Logger::default();
        web::App::new()
            .wrap(Error)
            .wrap(logger)
            .state(AppName {
                name: String::from("toynetwork"),
            })
            .state(counter.to_owned())
            .state(ro_counter.to_owned())
            .configure(scoped_config)
            .route("/stream", web::get().to(sse))
            .service(index)
            .service(hello)
            .service(echo)
            .service(increment_counter)
            .service(handle_json)
            .service(show_read_only_app_state)
            .service(add_one)
            .service(handle_error)
            .service(web::scope("/app").route("/hey", web::get().to(manual_hello)))
            .service(web::resource("/{filename}.*").route(web::get().to(file_server)))
            .service(
                ntex_files::Files::new("/static/", ".")
                    .show_files_listing()
                    .use_last_modified(true),
            )
            .service(web::resource("/ferr").route(
                web::get().to(|| async { web::HttpResponse::InternalServerError().finish() }),
            ))
    })
    .bind_openssl("0.0.0.0:8080", builder)?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use super::*;
    use futures::{future, Stream, StreamExt};
    use ntex::web::test;

    #[ntex::test]
    async fn test_stream_chunk() {
        let app = test::init_service(web::App::new().route("/", web::get().to(sse))).await;

        let req = test::TestRequest::get().to_request();

        let mut res = test::call_service(&app, req).await;

        assert!(res.status().is_success());

        let mut body = Box::pin(res.take_body().into_body::<Bytes>());

        let bytes = future::poll_fn(|cx| body.as_mut().poll_next(cx)).await;

        assert_eq!(bytes.unwrap().unwrap(), Bytes::from_static(b"data: 5\r\n"));

        let bytes = future::poll_fn(|cx| body.as_mut().poll_next(cx)).await;

        #[should_panic]
        assert_eq!(bytes.unwrap().unwrap(), Bytes::from_static(b"data: 4\r\n"));

        for i in 0..3 {
            let expected_data = format!("data: {}\r\n", 3 - i);

            let bytes = future::poll_fn(|cx| body.as_mut().poll_next(cx)).await;

            assert_eq!(bytes.unwrap().unwrap(), Bytes::from(expected_data));
        }
    }

    #[ntex::test]
    async fn test_stream_full_payload() {
        let app = test::init_service(web::App::new().route("/", web::get().to(sse))).await;

        let req = test::TestRequest::get().to_request();

        let mut resp = test::call_service(&app, req).await;

        assert!(resp.status().is_success());

        let body = resp.take_body().into_body::<Bytes>();

        let bytes = body
            .fold(ntex::util::BytesMut::new(), |mut acc, chunk| async move {
                acc.extend_from_slice(&chunk.unwrap());
                acc
            })
            .await;

        assert_eq!(
            bytes,
            Bytes::from_static(b"data: 5\r\ndata: 4\r\ndata: 3\r\ndata: 2\r\ndata: 1\r\n")
        );
    }
}
