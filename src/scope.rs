use ntex::web;

pub fn scoped_config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/api")
            .route(web::get().to(|| async { web::HttpResponse::Ok().body("API") })),
    );
}
